import { Component, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import { map, mergeMap,filter } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestApiService } from '../shared/rest-api.service';
import { Shipper } from '../shared/shipper';

@Component({
  selector: 'app-shipper-list',
  templateUrl: './shipper-list.component.html',
  styleUrls: ['./shipper-list.component.css']
})
export class ShipperListComponent implements OnInit {

  Shippers: any = [];
  constructor( 
    public restApi: RestApiService,
    public http: HttpClient
    ) { }

  ngOnInit(): void {
    this.loadShippers();
  }

  loadShippers() {
    return this.restApi.getShippers().subscribe((data: {}) => {
        this.Shippers = data;
    })
  }

  deleteShipper(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteShipper(id).subscribe(data => {
        this.loadShippers()
      })
    }
  }  

}
