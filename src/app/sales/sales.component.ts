import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { BubbleController, Chart, registerables } from 'chart.js';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  result: any;
  category: any;
  sales: any;
  chart: any = [];

  constructor(private service:RestApiService) {
    Chart.register(...registerables);
   }

  ngOnInit(): void {
    this.service.cryptoData().then((res:any) => {
      this.result = res;
      this.category = this.result.map((item: any) => item.category);
      this.sales = this.result.map((item: any) => item.sales);
      console.log(this.category);
      console.log(this.sales);

      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: this.category,
          datasets: [
            {
              data: this.sales,
              borderColor: '#3e95cd',
              label:'Category Sales',
              backgroundColor: 'pink',
              borderWidth: 3,
            },
          ],
        },
      });
    });
    
  }

}
